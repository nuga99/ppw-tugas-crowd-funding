from django.contrib import admin
from .models import CrowdFundingUser
# Register your models here.

class CrowdFundingUserAdmin(admin.ModelAdmin): # pragma: no cover
    list_display = ('username', 'nama' , 'tanggal_lahir', 'email', 'password')
    list_display_links = ('nama',)
    search_fields = ('username','nama')


admin.site.register(CrowdFundingUser,CrowdFundingUserAdmin) # pragma: no cover
# Register your models here.
