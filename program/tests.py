from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import *
from .forms import *
from django.apps import apps
from program.apps import ProgramConfig

# Create your tests here.
class UnitTest(TestCase): # pragma: no cover

    def test_donate_page_exists(self):
        response = Client().get('/donate')
        self.assertEqual(response.status_code, 200)

    def test_donate_page_exists_using_index_func(self):
        found = resolve('/donate')
        self.assertEqual(found.func, index_donate)

    def test_model_can_create_new_donation(self):
        new_donation = DonasiProgramUser.objects.create(
            nama_program = "test",
            nama = "test",
            email = "test@yahoo.com",
            uang_donasi = 8,
            nama_donatur = "Test"
            )
        counting_all_available_donation = DonasiProgramUser.objects.all().count()
        self.assertEqual(counting_all_available_donation, 1)

    def test_form_validation_for_blank_items(self):
            form = DonationForm(data={'PILIHAN_PROGRAM' : '',
                                      'nama_program' : '',
                                      'nama' : '',
                                      'email' : '',
                                      'uang_donasi' : 0,
                                      'nama_donatur' : ''}
                                )
            self.assertFalse(form.is_valid())
            self.assertEqual(
                form.errors['nama_program'],
                ["This field is required."]
                )
            self.assertEqual(
                form.errors['nama'],
                ["This field is required."]
                )
            self.assertEqual(
                form.errors['email'],
                ["This field is required."]
                )
            self.assertEqual(
                form.errors['uang_donasi'],
                ["Ensure this value is greater than or equal to 1."]
                )
            self.assertTrue(form['nama_donatur'],False)


    #unit test bagian check user
    
    # def test_form_donation_valid(self):
    #     form = DonationForm(data={'nama_program':'Nama Program',
    #                         'nama':'Jonsky',
    #                         'email':'danur@gmail.com',
    #                         'uang_donasi': 1000000,
    #                         'nama_donatur':'Sri dodo'})
    #     self.assertTrue(form.is_valid())

    # def test_form_donation_not_valide(self):
    #     form = DonationForm(data={})
    #     self.assert(form.is_valid())


    # def test_user_email_exist(self):
    #     response = CrowdFundingUser.objects.create(email="test@gmail.com")
    #     self.assertTrue(response)

    # def test_user_email_no_exist(self):
    #     response = CrowdFundingUser.objects.create(email="test@gmail.com")
    
    def test_apps(self):
        self.assertEqual(ProgramConfig.name, 'program')
        self.assertEqual(apps.get_app_config('program').name, 'program')