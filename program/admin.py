from django.contrib import admin # pragma: no cover
from .models import Program, Berita , DonasiProgramUser # pragma: no cover

class ProgramAdmin(admin.ModelAdmin): # pragma: no cover
    list_display = ('nama_program', 'link_gambar', 'deskripsi_program', 'uang_donasi')
    list_display_links = ('nama_program',)
    search_fields = ('nama_program',)

class DonasiProgramUserAdmin(admin.ModelAdmin): # pragma: no cover
    list_display = ('nama_program', 'nama', 'email', 'uang_donasi','nama_donatur')
    list_display_links = ('nama_program','nama_donatur')
    search_fields = ('nama_program','nama_donatur')

class BeritaAdmin(admin.ModelAdmin): # pragma: no cover
    list_display = ('judul_berita', 'link_gambar_berita', 'deskripsi_berita')
    list_display_links = ('judul_berita',)
    search_fields = ('judul_berita',)

admin.site.register(Program, ProgramAdmin) # pragma: no cover
admin.site.register(DonasiProgramUser, DonasiProgramUserAdmin) # pragma: no cover
admin.site.register(Berita,BeritaAdmin) # pragma: no cover 
#nanti tambahin
