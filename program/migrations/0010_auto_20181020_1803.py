# Generated by Django 2.1.1 on 2018-10-20 11:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('program', '0009_auto_20181018_0045'),
    ]

    operations = [
        migrations.AlterField(
            model_name='berita',
            name='deskripsi_berita',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='berita',
            name='link_gambar_berita',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='program',
            name='deskripsi_program',
            field=models.TextField(),
        ),
    ]
