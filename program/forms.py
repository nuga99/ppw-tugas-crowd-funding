from django import forms
from program.models import Program , DonasiProgramUser

class DonationForm(forms.Form):

    PILIHAN_PROGRAM = (
        ('Panti Asuhan Budi Mulya', 'Panti Asuhan Budi Mulya'),
        ('Panti Asuhan Sahabat Anak Indonesia', 'Panti Asuhan Sahabat Anak Indonesia'),
        ('Gempa Bumi Situbondo', 'Gempa Bumi Situbondo'),
        ('Tsunami Palu', 'Tsunami Palu'),
        ('Lingkungan Kumuh di Palembang', 'Lingkungan Kumuh di Palembang'),
        ('Menggalang Dana Peduli Anak Foundation', 'Menggalang Dana Peduli Anak Foundation'),
        ('Wabah Penyakit', 'Wabah Penyakit')
    )
    nama_program = forms.ChoiceField(choices = PILIHAN_PROGRAM)
    nama = forms.CharField(max_length = 200, widget=forms.TextInput(
        attrs={
        'class':'form-control',
        'placeholder':'masukkan nama'
        }
    ))
    email = forms.EmailField(max_length = 150, widget=forms.TextInput(
        attrs={
        'class':'form-control',
        'placeholder':'masukkan email'
        }
    ))

    uang_donasi = forms.IntegerField(min_value = 1, widget=forms.TextInput(
        attrs={
        'class':'form-control',
        'placeholder':'masukkan besaran donasi'
        }
    ))

    nama_donatur = forms.BooleanField(required=False)
