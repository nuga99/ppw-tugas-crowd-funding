from django.shortcuts import render, redirect
from program.models import Program, Berita , DonasiProgramUser
from authenticate.models import CrowdFundingUser
from program.forms import DonationForm
from django.http import HttpResponse
from django.contrib import messages
from django.db.models import F

# Buat munculin program" di html
def index_list_program(request):
    list_of_programs = Program.objects.all()
    form = DonationForm()
    args = {'list_of_programs' : list_of_programs, 'form' : form}
    return render(request, 'program/program.html' , args)


def index_list_berita(request):
	list_of_berita = Berita.objects.all()
	args = {'list_of_berita' : list_of_berita}
	return render(request, 'program/news.html', args)

def index_donate(request): # pragma: no cover
    form = DonationForm()
    if(request.method == "POST"):
        form = DonationForm(request.POST)

        user_email = request.POST.get('email')

        try:
            user_get = CrowdFundingUser.objects.get(email=user_email)

        except CrowdFundingUser.DoesNotExist:
            user_get = None

        # check email jika belum terdaftar
        if(user_get == None): 
            messages.error(request, "Email belum terdaftar.")
            return redirect('donate')


        elif form.is_valid():
            nama_program_pilihan = request.POST.get('nama_program')
            email = request.POST.get('email')
            nama = request.POST.get('nama')
            uang = request.POST.get('uang_donasi')
            nama_donatur = request.POST.get('nama_donatur')

            # cek nama donatur anonim
            if(nama_donatur != None):
                data_donasi = DonasiProgramUser(nama_program=nama_program_pilihan,
                                                nama=nama,email=email,
                                                uang_donasi=uang,
                                                nama_donatur=True)
            # cek nama donatur tidak anonim
            else:
                data_donasi = DonasiProgramUser(nama_program=nama_program_pilihan,
                                                nama=nama,email=email,
                                                uang_donasi=uang,
                                                nama_donatur=False)

            # update nilai program
            Program.objects.filter(nama_program  = nama_program_pilihan).update(uang_donasi=F('uang_donasi')+uang)

            data_donasi.save()

            messages.success(request, 'Selamat donasi telah ditambahkan')
            return redirect('donate')
        else:
            form = DonationForm()

    return render(request, 'program/donate.html',  {'form':form})

