# Tugas Kelompok PPW

Tugas KELOMPOK C13 - Crowd Funding Apps

## Nama-nama anggota kelompok

Gema Pratama Aditya - 1706040031 <br />
Sayid Abyan Rizal Shiddiq - 1706022445 <br />
Muhammad Ardivan Satrio Nugroho - 1706025361

## Status pipelines:

[![pipeline status](https://gitlab.com/ppw_senang/ppw-tugas-crowd-funding/badges/master/pipeline.svg)](https://gitlab.com/ppw_senang/ppw-tugas-crowd-funding/commits/master)

## Status code coverage:

[![coverage report](https://gitlab.com/ppw_senang/ppw-tugas-crowd-funding/badges/master/coverage.svg)](https://gitlab.com/ppw_senang/ppw-tugas-crowd-funding/commits/master)


## Link heroku:

```
http://ppw-berdonasi.herokuapp.com/
```
