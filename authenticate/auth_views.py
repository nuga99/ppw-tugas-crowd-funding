from django.contrib.auth import authenticate,logout
from django.contrib.auth import login
from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import SignUpForm
from django.contrib.auth import get_user_model
from .models import CrowdFundingUser
from django.contrib import messages
from django.urls import reverse

# Create your views here.

# Method for sign up
def signup(request):
    if(request.method == 'POST'):
        form_register = SignUpForm(request.POST)
        if form_register.is_valid():
            # must  implement parameter username
            form_register.save()
            return redirect('login')
    else:
        form_register = SignUpForm()
        
    return render(request, 'signup.html', {'form': form_register})


# login function from sayid html
def login_views(request):
    if request.method == 'POST':

        user_email = request.POST.get('email')

        # query get objects try except
        try:
            user_get = CrowdFundingUser.objects.get(email=user_email)

        except CrowdFundingUser.DoesNotExist:
            user_get = None

        if(user_get == None):
            messages.error(request, "Username atau password salah.")
            return render(request,'login.html')

        user_name = user_get.username # query get username from User
        user_password = request.POST.get('password')
        
        user = authenticate(request,username=user_name, password=user_password)

        if user is not None:
            if user.is_active:
                login(request,user)
                return redirect('dashboard') # return dashboard homepage

            else:
                return HttpResponse('Akun tidak ada.')  

        else:
            messages.error(request, "Username atau password salah.")
            return render(request,'login.html')

    else:
        return render(request,'login.html')

# logout function
def logout_views(request):
    logout(request)
    messages.success(request, 'Berhasil keluar dari laman')
    return redirect('login')
