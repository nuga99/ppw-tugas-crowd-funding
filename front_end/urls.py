from django.urls import path
from front_end import views

urlpatterns = [
    path('', views.dashboard, name='dashboard'),
    # path('daftar_donator/', views.donator, name= "donator"),
    path('profil/', views.profile, name= "profile"),
    path('tentang_kami/', views.aboutUs, name= "aboutUs"),

    path('news1/', views.news1, name = "news1"),
    path('news2/', views.news2, name = "news2"),
    path('news3/', views.news3, name = "news3"),
    path('news4/', views.news4, name = "news4"),
    path('news5/', views.news5, name = "news5"),
    path('news6/', views.news6, name = "news6")
    #path('berita/', views.news, name= "news"),

    #Dari gema

    # path('Masuk', views.login, name="login"),
    # path('Daftar', views.register, name= "register"),
]
