from django.test import TestCase, Client
from django.urls import resolve
from .models import CrowdFundingUser
from authenticate import auth_views
from django.apps import apps
from authenticate.apps import AuthenticateConfig

# Create your tests here.
client = Client()  # set client from class Client

class AuthenticateTest(TestCase):  # pragma: no cover

    def test_urls_sign_up_is_exist(self):
        response = client.get('/daftar/')
        self.assertEqual(response.status_code, 200)

    def test_urls_sign_in_is_exist(self):
        response = client.get('/masuk/')
        self.assertEqual(response.status_code, 200)

    def test_urls_sign_out_is_exist(self):
        response = client.get('/keluar/')
        self.assertEqual(response.status_code, 302)  # redirect

    # check user is_valid
    def test_models_user_created(self):  # pragma: no cover
        create_user = CrowdFundingUser.objects.create(nama="Nuga", tanggal_lahir="2018-10-16",
                                                      email="ppw_donasi@donasi.com", password="i34y8132hewjaaendjkdnajda")
        count_user = CrowdFundingUser.objects.all().count()
        self.assertEqual(count_user, 1)

    # check test views sign_up
    def test_sign_up_using_function(self):
        response = resolve('/daftar/')
        self.assertEqual(response.func, auth_views.signup)

    # check test views login
    def test_login_using_function(self):
        response = resolve('/masuk/')
        self.assertEqual(response.func, auth_views.login_views)

    # check test function views logout
    def test_logout_using_function(self):
        response = resolve('/keluar/')
        self.assertEqual(response.func, auth_views.logout_views)

    # check post request sign up atau daftar
    def test_sign_up_POST_request(self):
        response = client.post('/daftar/',
                               data={'username': 'john',
                                     'nama': 'ManusiaABCDEFG',
                                     'tanggal_lahir': '1999-10-09',
                                     'email': 'wkwkwkwk@gmail.com',
                                     'password1': 'jkadbuasbdks',
                                     'password2': 'jkadbuasbdks'
                                     })
        count_all_user = CrowdFundingUser.objects.all().count()
        self.assertEqual(count_all_user, 1)
        self.assertEqual(response.status_code, 302)  # redirect status

    # check post request saat login atau masuk
    def test_sign_in_POST_request(self):
        response = client.post('/masuk/',
                               data={'email': 'nuga@gmail.com', 'password': 'nnjkasand'})
        count_all_user_login = CrowdFundingUser.objects.all().count()
        self.assertEqual(count_all_user_login, 0)
        self.assertEqual(response.status_code, 200)  # redirect
        #self.assertEqual(response['location'], '')

    # check post request fail
    def test_sign_in_POST_request_fail(self):
        response = client.post('/masuk/', data={})
        count_all_user_login = CrowdFundingUser.objects.all().count()
        self.assertEqual(count_all_user_login, 0)
        self.assertEqual(response.status_code, 200)

    # check query get username
    def test_user_query_get_name(self):
        pass
        # response = CrowdFundingUser.objects.create(
        #     'username': 'john',
        #     'nama': 'ManusiaABCDEFG',
        #     'tanggal_lahir': '1999-10-09',
        #     'email': 'wkwkwkwk@gmail.com',
        #     'password1': 'jkadbuasbdks',
        #     'password2': 'jkadbuasbdks'
        # )
        # self.assertIsNotNone(response.context['username'])

    # check name apps
    def test_apps(self):
        self.assertEqual(AuthenticateConfig.name, 'authenticate')
        self.assertEqual(apps.get_app_config('authenticate').name, 'authenticate')
