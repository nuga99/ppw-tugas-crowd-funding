from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model

User = get_user_model()

class SignUpForm(UserCreationForm):
    # nama = forms.CharField(max_length=100, required=True)
    # tanggal_lahir = forms.DateField()
    # email = forms.EmailField(help_text="Mohon masukan email yang valid.")
    class Meta:
        model = User
        fields = ['username','nama','tanggal_lahir' ,'email']
        widgets = {

            'username': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'masukkan username', 'maxlength':150}),

            'nama': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'masukkan nama', 'maxlength':150}),

            'tanggal_lahir': forms.DateInput(attrs={'type': 'date','class': 'form-control'}),

            'email': forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'masukkan email', 'maxlength':150}),
        }
